#![deny(unsafe_code)]
#![deny(clippy::all)]
#![deny(clippy::pedantic)]

pub use owned_subset_derive::OwnedSubset;

pub trait OwnedSubset<Parent> {
    fn from_parent(parent: Parent) -> Self;
}

impl<T> OwnedSubset<T> for T {
    fn from_parent(parent: T) -> Self {
        parent
    }
}
