use owned_subset::OwnedSubset;

#[derive(Debug)]
enum Whole {
    A {
        foo: &'static str,
        bar: &'static str,
        baz: &'static str,
    },
    B {
        ham: &'static str,
        egg: &'static str,
    },
}

#[derive(Debug, Eq, PartialEq, OwnedSubset)]
#[owned_subset(parent = Whole)]
enum Subset {
    A {
        foo: &'static str,
        baz: &'static str,
    },
    B {
        ham: &'static str,
    },
}

#[test]
fn struct_generics() {
    assert_eq!(
        Subset::from_parent(Whole::A {
            foo: "foo",
            bar: "bar",
            baz: "baz",
        }),
        Subset::A {
            foo: "foo",
            baz: "baz",
        }
    );
    assert_eq!(
        Subset::from_parent(Whole::B {
            ham: "ham",
            egg: "egg"
        }),
        Subset::B { ham: "ham" }
    );
}
