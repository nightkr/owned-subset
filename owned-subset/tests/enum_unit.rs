use owned_subset::OwnedSubset;

#[allow(dead_code)]
enum Whole {
    A,
    B,
}

#[allow(dead_code)]
#[derive(OwnedSubset)]
#[owned_subset(parent = Whole)]
enum Subset {
    A,
    B,
}

fn main() {}
