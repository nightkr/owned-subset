use owned_subset::OwnedSubset;

#[derive(Debug)]
struct Whole<T> {
    foo: T,
    bar: T,
    baz: T,
}

#[derive(Debug, Eq, PartialEq, OwnedSubset)]
#[owned_subset(parent = Whole<T>)]
struct Subset<T> {
    foo: T,
    baz: T,
}

#[test]
fn struct_generics() {
    assert_eq!(
        Subset::from_parent(Whole {
            foo: "foo",
            bar: "bar",
            baz: "baz",
        }),
        Subset {
            foo: "foo",
            baz: "baz",
        }
    );
}
