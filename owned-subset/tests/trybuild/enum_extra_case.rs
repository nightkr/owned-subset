use owned_subset::OwnedSubset;

enum Whole {
    A { foo: &'static str },
}

#[derive(OwnedSubset)]
#[owned_subset(parent = Whole)]
enum Subset {
    A { foo: &'static str },
    B { foo: &'static str },
}

fn main() {}
