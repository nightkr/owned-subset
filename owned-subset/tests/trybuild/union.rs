use owned_subset::OwnedSubset;

struct Whole {}

#[derive(OwnedSubset)]
#[owned_subset(parent = Whole)]
union Subset {
    foo: u8,
}

fn main() {}
