use owned_subset::OwnedSubset;

struct Whole {
    foo: &'static str,
}

#[derive(OwnedSubset)]
#[owned_subset(parent = Whole)]
struct Subset {
    foo: &'static str,
    bar: &'static str,
}

fn main() {}
