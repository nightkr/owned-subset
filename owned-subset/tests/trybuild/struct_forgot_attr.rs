use owned_subset::OwnedSubset;

#[derive(OwnedSubset)]
struct Subset {
    foo: &'static str,
    baz: &'static str,
}

fn main() {}
