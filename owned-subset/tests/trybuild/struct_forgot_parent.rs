use owned_subset::OwnedSubset;

#[derive(OwnedSubset)]
#[owned_subset()]
struct Subset {
    foo: &'static str,
    baz: &'static str,
}

fn main() {}
