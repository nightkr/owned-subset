use owned_subset::OwnedSubset;

#[derive(Debug)]
struct WholeOuter {
    inner: WholeInner,
}

#[derive(Debug)]
struct WholeInner {
    foo: &'static str,
    bar: &'static str,
    baz: &'static str,
}

#[derive(Debug, Eq, PartialEq, OwnedSubset)]
#[owned_subset(parent = WholeOuter)]
struct SubsetOuter {
    inner: SubsetInner,
}

#[derive(Debug, Eq, PartialEq, OwnedSubset)]
#[owned_subset(parent = WholeInner)]
struct SubsetInner {
    foo: &'static str,
    baz: &'static str,
}

#[test]
fn struct_nested() {
    assert_eq!(
        SubsetOuter::from_parent(WholeOuter {
            inner: WholeInner {
                foo: "foo",
                bar: "bar",
                baz: "baz",
            }
        }),
        SubsetOuter {
            inner: SubsetInner {
                foo: "foo",
                baz: "baz",
            }
        }
    );
}
