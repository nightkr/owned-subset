use owned_subset::OwnedSubset;

#[derive(Debug)]
struct Whole {
    foo: &'static str,
    bar: &'static str,
    baz: &'static str,
}

#[derive(Debug, Eq, PartialEq, OwnedSubset)]
#[owned_subset(parent = Whole)]
struct Subset {
    foo: &'static str,
    baz: &'static str,
}

#[test]
fn struct_simple() {
    assert_eq!(
        Subset::from_parent(Whole {
            foo: "foo",
            bar: "bar",
            baz: "baz",
        }),
        Subset {
            foo: "foo",
            baz: "baz",
        }
    );
}
