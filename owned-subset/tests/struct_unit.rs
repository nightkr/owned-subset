use owned_subset::OwnedSubset;

struct Whole;

#[derive(OwnedSubset)]
#[owned_subset(parent = Whole)]
struct Subset;

fn main() {}
