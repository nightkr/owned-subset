#![deny(unsafe_code)]
#![deny(clippy::all)]
#![deny(clippy::pedantic)]
#![allow(clippy::filter_map)]

use proc_macro2::{Span, TokenStream};
use quote::quote;
use std::iter::Iterator;
use syn::{parse::Parse, AttrStyle, DeriveInput, Fields, Path, PathArguments, PathSegment, Token};

#[derive(Debug)]
struct OwnedSubsetAttrs {
    parent: Path,
}

impl Parse for OwnedSubsetAttrs {
    fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
        let mut parent = None;
        while !input.is_empty() {
            let path = input.parse::<Path>()?;
            if path.is_ident("parent") {
                input.parse::<Token![=]>()?;
                parent = Some(input.parse()?);
            } else {
                return Err(syn::Error::new_spanned(path, "unknown attribute"));
            }
            if !input.is_empty() {
                input.parse::<Token![,]>()?;
            }
        }
        Ok(Self {
            parent: parent.ok_or_else(|| {
                input.error("must specify parent using #[owned_subset(parent = ...)]")
            })?,
        })
    }
}

fn subpath(path: impl Into<Path>, item: impl Into<PathSegment>) -> Path {
    let mut path = path.into();
    path.segments.push(item.into());
    path
}

fn strip_path_arguments(path: &mut Path) {
    for segment in path.segments.iter_mut() {
        segment.arguments = PathArguments::None;
    }
}

fn derive_convert_variant_arm(mut parent: Path, mut subset: Path, fields: &Fields) -> TokenStream {
    strip_path_arguments(&mut parent);
    strip_path_arguments(&mut subset);
    let field_names = fields.iter().map(|field| &field.ident);
    let field_mappings = field_names
        .clone()
        .map(|name| quote! { #name: ::owned_subset::OwnedSubset::from_parent(#name) });
    quote! { #parent { #(#field_names,)* .. } => #subset { #(#field_mappings),* } }
}

fn derive_owned_subset(
    OwnedSubsetAttrs { parent }: OwnedSubsetAttrs,
    input: &DeriveInput,
) -> syn::Result<TokenStream> {
    let subset_ident = &input.ident;
    let from_parent_arms = match &input.data {
        syn::Data::Struct(data) => {
            derive_convert_variant_arm(parent.clone(), subset_ident.clone().into(), &data.fields)
        }
        syn::Data::Enum(data) => {
            let arms = data.variants.iter().map(|variant| {
                derive_convert_variant_arm(
                    subpath(parent.clone(), variant.ident.clone()),
                    subpath(subset_ident.clone(), variant.ident.clone()),
                    &variant.fields,
                )
            });
            quote! { #(#arms),* }
        }
        // Can't support unions, since we need to know the case to convert correctly
        syn::Data::Union(_) => {
            return Err(syn::Error::new(
                Span::call_site(),
                "this trait cannot be derived for unions",
            ))
        }
    };
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    Ok(quote! {
        #[automatically_derived]
        impl #impl_generics ::owned_subset::OwnedSubset<#parent> for #subset_ident #ty_generics #where_clause {
            fn from_parent(parent: #parent) -> Self {
                match parent { #from_parent_arms }
            }
        }
    })
}

#[proc_macro_derive(OwnedSubset, attributes(owned_subset))]
pub fn owned_subset(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input: DeriveInput = syn::parse_macro_input!(input as DeriveInput);
    let output = input
        .attrs
        .iter()
        .filter(|attr| attr.style == AttrStyle::Outer && attr.path.is_ident("owned_subset"))
        .map(|attr| {
            attr.parse_args::<OwnedSubsetAttrs>()
                .and_then(|attrs| derive_owned_subset(attrs, &input))
                .unwrap_or_else(|err| syn::Error::to_compile_error(&err))
        })
        .collect::<TokenStream>();
    if output.is_empty() {
        quote! { compile_error!("derive(OwnedSubset) requires #[owned_subset(...)] attr"); }
    } else {
        output
    }
    .into()
}
